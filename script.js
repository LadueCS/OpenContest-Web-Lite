function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
  return 0;
}

function loginBar() {
  const username = getCookie("username");
  if (username == 0) {
    document.getElementById("login-bar").innerHTML = `<a href="login" class="btn btn-primary me-2">Login</a>
    <a href="register" class="btn btn-outline-primary me-2">Register</a>`
  }
  else {
    document.getElementById("login-bar").innerHTML = `<a href="logout" class="btn btn-primary me-2">Logout</a>`
  }
}

function authenticate(server) {
  const reqBody = {
    type: "authenticate",
    username: getCookie("username"),
    password: getCookie("password"),
    server: server
  };

  let ret = "";

  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState = 4) {
      if (this.status == 200) {
	ret = xhr.responseText.slice(1, -1);
      }
    }
  }
  
  xhr.open("POST", getCookie("homeserver"), false);
  xhr.send(JSON.stringify(reqBody));

  return ret;
}
